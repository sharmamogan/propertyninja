﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PropertyNinja.Models;

namespace PropertyNinja.Data
{
    public class PropertyNinjaContext : DbContext
    {
        public PropertyNinjaContext (DbContextOptions<PropertyNinjaContext> options)
            : base(options)
        {
        }

        public DbSet<PropertyNinja.Models.Property> Property { get; set; }
    }
}
