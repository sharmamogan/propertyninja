﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PropertyNinja.Data;
using PropertyNinja.Models;
using X.PagedList.Mvc.Core;
using X.PagedList;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace PropertyNinja.Controllers
{
    public class BuyerListingController : Controller
    {
        private readonly PropertyNinjaContext _context;

        public BuyerListingController(PropertyNinjaContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(string searchString, string sortBy, int? page)
        {
            var properties = from m in _context.Property
                             select m;

            ViewBag.Count = properties.Count();

            //Search based on input if searchString not empty.
            if (!String.IsNullOrEmpty(searchString))
            {
                properties = properties.Where(s => s.PropertyType.Contains(searchString) 
                                                || s.PropertyName.Contains(searchString) 
                                                || s.PropertyAddress.Contains(searchString)
                                                 || s.PropertyPrice.ToString().Contains(searchString)
                                                || searchString == null);
                
                ViewBag.Count = properties.Count();
            }
            else
            {
                //Runs sorting if searchString empty and search button is clicked
                if (sortBy == "priceLow")
                {
                    properties = from s in _context.Property
                                 orderby s.PropertyPrice ascending
                                 select s;
                }
                else if (sortBy == "priceHigh")
                {
                    properties = from s in _context.Property
                                 orderby s.PropertyPrice descending
                                 select s;
                }
                else if (sortBy == "landLow")
                {
                    properties = from s in _context.Property
                                 orderby s.PropertyLandSize ascending
                                 select s;
                }
                else if (sortBy == "landHigh")
                {
                    properties = from s in _context.Property
                                 orderby s.PropertyLandSize descending
                                 select s;
                }
                else if (sortBy == "bedroomLow")
                {
                    properties = from s in _context.Property
                                 orderby s.PropertyBedroom ascending
                                 select s;
                }
                else if (sortBy == "bedroomHigh")
                {
                    properties = from s in _context.Property
                                 orderby s.PropertyBedroom descending
                                 select s;
                }
            }

            //Returns the properties in a paged list
            return View(properties.ToList().ToPagedList(page ?? 1, 6));
        }

        [Authorize]
        public async Task<IActionResult> PropertyDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Property
                .FirstOrDefaultAsync(m => m.ID == id);

            if (@property == null)
            {
                return NotFound();
            }

            ViewBag.images = GetBlobs(@property.PropertyName.ToLower().Replace(",", "").Replace(" ", "-"));


            return View(@property);
        }

        //GET Blob storage info
        private CloudBlobClient getBlobStorageInformation()
        {
            //read json
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json");
            IConfigurationRoot configure = builder.Build();

            //to get key access
            //once link, time to read the content to get the connectionstring
            CloudStorageAccount objectaccount = CloudStorageAccount.Parse(configure["Connectionstrings:storageconnection"]);

            CloudBlobClient blobclientagent = objectaccount.CreateCloudBlobClient();

            return blobclientagent;
        }

        //Function to get blobs based on the property name
        public List<string> GetBlobs(string name)
        {
            try
            {
                CloudBlobClient client = getBlobStorageInformation();

                //Creates a container using property name entered by seller
                CloudBlobContainer container = client.GetContainerReference(name);

                //create empty list to store for the blobs list information
                List<string> blobs = new List<string>();

                //get the listing record from the blob storage
                BlobResultSegment result = container.ListBlobsSegmentedAsync(null).Result;

                //read blob listing from the storage
                foreach (IListBlobItem item in result.Results)
                {
                    //check thge type of blob : block blob or directory or page block
                    if (item.GetType() == typeof(CloudBlockBlob))
                    {
                        CloudBlockBlob blob = (CloudBlockBlob)item;
                        blobs.Add(blob.Name + "#" + blob.Uri.ToString());
                    }
                    else if (item.GetType() == typeof(CloudBlobDirectory))
                    {
                        CloudBlobDirectory blob = (CloudBlobDirectory)item;
                        blobs.Add(blob.Uri.ToString());
                    }
                }

                return blobs;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //To view seller details (might need to move to other user related controller)
        public async Task<IActionResult> ViewSeller(string sellerName, int? page)
        {
            ViewBag.sellerName = sellerName;

            var properties = from m in _context.Property
                             select m;
            
            if (!String.IsNullOrEmpty(sellerName))
            {
                properties = properties.Where(s => s.PropertyType.Equals(sellerName));

                ViewBag.Count = properties.Count();
            }

            return View(properties.ToList().ToPagedList(page ?? 1, 3));
        }
    }
}
