﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PropertyNinja.Data;
using PropertyNinja.Models;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using PropertyNinja.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using PropertyNinja.Controllers;
using Microsoft.Azure.ServiceBus;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace PropertyNinja.Views.Properties
{
    [Authorize]
    public class PropertiesController : Controller
    {
        private readonly PropertyNinjaContext _context;
        private readonly UserManager<PropertyNinjaUser> _userManager;

        public PropertiesController(PropertyNinjaContext context, UserManager<PropertyNinjaUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        //GET Blob storage info
        public CloudBlobClient getBlobStorageInformation()
        {
            //read json
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json");
            IConfigurationRoot configure = builder.Build();

            //to get key access
            //once link, time to read the content to get the connectionstring
            CloudStorageAccount objectaccount = CloudStorageAccount.Parse(configure["Connectionstrings:storageconnection"]);

            CloudBlobClient blobclientagent = objectaccount.CreateCloudBlobClient();

            return blobclientagent;
        }

        // GET: Properties
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            var properties = from m in _context.Property
                             select m;

            if (user != null)
            {
                var name = user.Name;
                properties = properties.Where(s => s.PropertyType.Equals(name));
            }

            return View(await properties.ToListAsync());
        }

        // GET: Properties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Property
                .FirstOrDefaultAsync(m => m.ID == id);
            if (@property == null)
            {
                return NotFound();
            }

            return View(@property);
        }

        // GET: Properties/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Properties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(List<IFormFile> files, [Bind("ID,PropertyType,PropertyName,PropertyAddress,PropertyBedroom,PropertyBathroom,PropertyFurnishing,PropertyDescription,PropertyLandSize,PropertySeller,PropertyPostedDate,PropertyPrice")] Property @property)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    CloudBlobClient client = getBlobStorageInformation();

                    //Creates a container using property name entered by seller
                    CloudBlobContainer container = client.GetContainerReference(@property.PropertyName.ToLower().Replace(",", "").Replace(" ", "-"));

                    container.CreateIfNotExists();

                    //Sends the blob parallely from the file list uploaded
                    Parallel.ForEach(files, file =>
                    {
                        string[] permittedExtensions = { ".jpg", ".jpeg", ".png" };

                        var ext = Path.GetExtension(file.FileName).ToLowerInvariant();

                        // To check file extension
                        if (string.IsNullOrEmpty(ext) || !permittedExtensions.Contains(ext))
                        {
                            // If extension is invalid
                            ViewBag.fileError = true;
                        }
                        else
                        {
                            //Blob name will be the file name
                            CloudBlockBlob blob = container.GetBlockBlobReference(file.FileName);

                            blob.UploadFromStreamAsync(file.OpenReadStream());
                        }
                    });
                }
                catch (Exception ex)
                {
                    throw;
                }

                if (ViewBag.fileError != true)
                {
                    //TO STORE THE THUMBNAIL IMAGE FOR PROPERTIES LISTING
                    @property.PropertyThumbnail = files.First().FileName;
                    @property.PropertyPostedDate = DateTime.Now;

                    //Send a service bus message
                    ServiceBusController.queueClient = new QueueClient(ServiceBusController.ServiceBusConnectionString, ServiceBusController.QueueName);
                    var qmessage = new Message(Encoding.UTF8.GetBytes(@property.PropertyName));
                    await ServiceBusController.queueClient.SendAsync(qmessage);

                    _context.Add(@property);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(@property);
        }

        // GET: Properties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Property.FindAsync(id);
            if (@property == null)
            {
                return NotFound();
            }
            return View(@property);
        }

        // POST: Properties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,PropertyType,PropertyName,PropertyAddress,PropertyBedroom,PropertyBathroom,PropertyFurnishing,PropertyDescription,PropertyLandSize,PropertySeller,PropertyPostedDate")] Property @property)
        {
            if (id != @property.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(@property);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PropertyExists(@property.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@property);
        }

        // GET: Properties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @property = await _context.Property
                .FirstOrDefaultAsync(m => m.ID == id);
            if (@property == null)
            {
                return NotFound();
            }

            return View(@property);
        }

        // POST: Properties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @property = await _context.Property.FindAsync(id);
            _context.Property.Remove(@property);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PropertyExists(int id)
        {
            return _context.Property.Any(e => e.ID == id);
        }
    }
}
