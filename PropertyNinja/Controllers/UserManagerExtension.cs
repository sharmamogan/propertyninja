﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PropertyNinja.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PropertyNinja.Controllers
{
    public static class UserManagerExtension
    {
        public static Task<PropertyNinjaUser> FindByNameAsync(this UserManager<PropertyNinjaUser> um, string name)
        {
            return um?.Users?.SingleOrDefaultAsync(x => x.Name == name);
        }
    }
}
