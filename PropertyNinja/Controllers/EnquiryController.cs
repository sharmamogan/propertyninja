﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using PropertyNinja.Models;
using PropertyNinja.Controllers;

namespace PropertyNinja.Controllers
{
    public class EnquiryController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        private CloudTable getTableInformation()
        {
            //step 1: read json
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            IConfigurationRoot configure = builder.Build();

            //to get key access
            //once link, time to read the content to get the connectionstring
            CloudStorageAccount storageaccount = CloudStorageAccount.Parse(configure["ConnectionStrings:storageconnection"]);

            CloudTableClient tableClient = storageaccount.CreateCloudTableClient();
            //step 2: how to create a new table in the storage.
            CloudTable table = tableClient.GetTableReference("EnquiryTable");

            return table;
        }

        //To send the enquiry information into enquiry table
        [HttpPost]
        public ActionResult addEntity(string sellerName, string buyerName, string phoneNumber, string email, string message, string propertyName)
        {
            CloudTable table = getTableInformation();

            table.CreateIfNotExists();

            TempData["enquiryMsg"] = null;

            try
            {
                EnquiryEntity enquiry = new EnquiryEntity(sellerName, DateTime.Now.Ticks.ToString("d19"));
                enquiry.BuyerName = buyerName;
                enquiry.PhoneNumber = phoneNumber;
                enquiry.Email = email;
                enquiry.Message = message;
                enquiry.PropertyName = propertyName;

                //Send a service bus message
                ServiceBusController.queueClient = new QueueClient(ServiceBusController.ServiceBusConnectionString, ServiceBusController.QueueName);
                var qmessage = new Message(Encoding.UTF8.GetBytes(buyerName));
                ServiceBusController.queueClient.SendAsync(qmessage);

                TableOperation insertOperation = TableOperation.Insert(enquiry);
                TableResult result = table.ExecuteAsync(insertOperation).Result;

                if (result.HttpStatusCode == 204)
                {
                    TempData["enquiryMsg"] = "Enquiry sent successfully to the seller!";
                }

            }
            catch (Exception ex)
            {
                TempData["enquiryMsg"] = ex.ToString();
            }

            return RedirectToAction("Index", "BuyerListing");
        }

        //Get all the result from table storage based on seller name
        public ActionResult GetEnquiries(string sellerName)
        {
            CloudTable table = getTableInformation();

            //Query to get data using partition key only
            TableQuery<EnquiryEntity> query = new TableQuery<EnquiryEntity>()
                    .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, sellerName));

            var result = table.ExecuteQuery(query);

            return View(result);
        }

        public ActionResult DeleteEntity(string PartitionKey, string RowKey)
        {
            CloudTable table = getTableInformation();

            TableOperation delete = TableOperation.Delete(new EnquiryEntity(PartitionKey, RowKey) { ETag = "*" });
            TableResult tableresult = table.ExecuteAsync(delete).Result;

            return RedirectToAction("GetEnquiry", "Enquiry");
        }
    }
}
