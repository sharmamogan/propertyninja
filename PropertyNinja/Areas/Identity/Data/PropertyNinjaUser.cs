﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace PropertyNinja.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the PropertyNinjaUser class
    public class PropertyNinjaUser : IdentityUser
    {
        public string Name { get; set; }
        public string Role { get; set; }
        public string ProfilePicture { get; set; }

    }
}
