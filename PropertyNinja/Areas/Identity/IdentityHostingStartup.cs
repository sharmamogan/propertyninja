﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PropertyNinja.Areas.Identity.Data;
using PropertyNinja.Data;

[assembly: HostingStartup(typeof(PropertyNinja.Areas.Identity.IdentityHostingStartup))]
namespace PropertyNinja.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<PropertyNinjaUserContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("PropertyNinjaUserContextConnection")));

                services.AddDefaultIdentity<PropertyNinjaUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<PropertyNinjaUserContext>();
            });
        }
    }
}