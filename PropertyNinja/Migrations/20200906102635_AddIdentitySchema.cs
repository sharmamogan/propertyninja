﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PropertyNinja.Migrations
{
    public partial class AddIdentitySchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PropertyPrice",
                table: "Property",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PropertyThumbnail",
                table: "Property",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PropertyPrice",
                table: "Property");

            migrationBuilder.DropColumn(
                name: "PropertyThumbnail",
                table: "Property");
        }
    }
}
