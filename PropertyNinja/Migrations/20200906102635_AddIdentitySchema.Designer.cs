﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PropertyNinja.Data;

namespace PropertyNinja.Migrations
{
    [DbContext(typeof(PropertyNinjaContext))]
    [Migration("20200906102635_AddIdentitySchema")]
    partial class AddIdentitySchema
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("PropertyNinja.Models.Property", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("PropertyAddress")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PropertyBathroom")
                        .HasColumnType("int");

                    b.Property<int>("PropertyBedroom")
                        .HasColumnType("int");

                    b.Property<string>("PropertyDescription")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PropertyFurnishing")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PropertyLandSize")
                        .HasColumnType("int");

                    b.Property<string>("PropertyName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("PropertyPostedDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("PropertyPrice")
                        .HasColumnType("int");

                    b.Property<string>("PropertySeller")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PropertyThumbnail")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PropertyType")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("Property");
                });
#pragma warning restore 612, 618
        }
    }
}
