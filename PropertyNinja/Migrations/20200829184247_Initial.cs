﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PropertyNinja.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Property",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyType = table.Column<string>(nullable: true),
                    PropertyName = table.Column<string>(nullable: true),
                    PropertyAddress = table.Column<string>(nullable: true),
                    PropertyBedroom = table.Column<int>(nullable: false),
                    PropertyBathroom = table.Column<int>(nullable: false),
                    PropertyFurnishing = table.Column<string>(nullable: true),
                    PropertyDescription = table.Column<string>(nullable: true),
                    PropertyLandSize = table.Column<int>(nullable: false),
                    PropertySeller = table.Column<string>(nullable: true),
                    PropertyPostedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Property", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Property");
        }
    }
}
