﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;

namespace PropertyNinja.Models
{
    public class EnquiryEntity:TableEntity
    {
        public EnquiryEntity() { }

        public EnquiryEntity(string SellerName, string SentDate)
        {
            this.PartitionKey = SellerName;
            this.RowKey = SentDate;
        }

        public string BuyerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public string PropertyName { get; set; }

    }
}
