﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertyNinja.Models
{
    public class Property
    {
        public int ID { get; set; }
        public string PropertyType { get; set; }
        public string PropertyName { get; set; }
        public string PropertyAddress { get; set; }
        public int PropertyBedroom { get; set; }
        public int PropertyBathroom { get; set; }
        public string PropertyFurnishing { get; set; }
        public string PropertyDescription { get; set; }
        public int PropertyLandSize { get; set; }
        public string PropertySeller { get; set; }
        public DateTime PropertyPostedDate { get; set; }
        public int PropertyPrice { get; set; }
        public string PropertyThumbnail { get; set; }
    }
}
